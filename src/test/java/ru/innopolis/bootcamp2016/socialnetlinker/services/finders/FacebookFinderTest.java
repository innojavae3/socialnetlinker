package ru.innopolis.bootcamp2016.socialnetlinker.services.finders;

import org.junit.Test;
import ru.innopolis.bootcamp2016.socialnetlinker.models.Account;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by lemhell on 19.07.16.
 */
public class FacebookFinderTest {
    @Test
    public void find() throws Exception {
        List<Account> accountList = new FacebookFinder().find("lemhell");
        assertTrue("There should be exactly 22 Emil Magerramovs on Facebook", accountList.size() == 22);
    }

}