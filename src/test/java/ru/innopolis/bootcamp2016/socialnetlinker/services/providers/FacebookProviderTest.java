package ru.innopolis.bootcamp2016.socialnetlinker.services.providers;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.innopolis.bootcamp2016.socialnetlinker.models.FieldType;

import java.util.Map;

public class FacebookProviderTest {
    private FacebookProvider sut;

    @Before
    public void before(){
        sut = new FacebookProvider();
    }

    @Test
    public void smokeTest(){
        Map<FieldType, Object> result = sut.provide("100009462662440");
        Assert.assertEquals("Олег", result.get(FieldType.FIRST_NAME));
        Assert.assertEquals("Кулаев", result.get(FieldType.SECOND_NAME));

//        Somehow does not work anymore
//        (I did not change anything, I swear!)
//        Assert.assertEquals("vilor9473@gmail.com", result.get(FieldType.EMAIL));
    }

}