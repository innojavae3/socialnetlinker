package ru.innopolis.bootcamp2016.socialnetlinker.services.providers;

import org.junit.Test;
import ru.innopolis.bootcamp2016.socialnetlinker.models.FieldType;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by lemhell on 21.07.16.
 */
public class GitHubProviderTest {
    @Test
    public void provide() throws Exception {
        Map<FieldType, Object> map = new GitHubProvider().provide("lemhell");
        assertTrue(map.containsKey(FieldType.FIRST_NAME));
        assertTrue(map.containsKey(FieldType.SECOND_NAME));
        assertTrue(map.containsKey(FieldType.PIC_GITHUB));
    }

}