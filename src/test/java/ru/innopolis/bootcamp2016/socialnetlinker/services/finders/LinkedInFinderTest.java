package ru.innopolis.bootcamp2016.socialnetlinker.services.finders;

import java.util.HashMap;
import java.util.Map;
import org.junit.Test;
import ru.innopolis.bootcamp2016.socialnetlinker.models.Account;
import static org.junit.Assert.assertEquals;

public class LinkedInFinderTest {

    @Test
    public void findByLogin() throws Exception {
        LinkedInFinder finder = new LinkedInFinder();

        Map<String, String> correctLinks = new HashMap<>();
        correctLinks.put("fchollet", "https://www.linkedin.com/in/fchollet");
        correctLinks.put("feeltheajf", "https://ru.linkedin.com/in/feeltheajf");
        correctLinks.put("segiddins", "https://www.linkedin.com/in/segiddins");
        correctLinks.put("borismus", "https://www.linkedin.com/in/borismus");


        for (String username : correctLinks.keySet()) {
            assertEquals(new Account(true, correctLinks.get(username)),
                    finder.find(username).get(0));
        }
    }

}