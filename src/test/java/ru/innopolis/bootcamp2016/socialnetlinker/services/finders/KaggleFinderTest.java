package ru.innopolis.bootcamp2016.socialnetlinker.services.finders;

import org.junit.Test;
import ru.innopolis.bootcamp2016.socialnetlinker.models.Account;

import static org.junit.Assert.assertEquals;

public class KaggleFinderTest {

    @Test
    public void findByLogin() throws Exception {
        KaggleFinder finder = new KaggleFinder();

        Account trueAccount = new Account(true, "https://www.kaggle.com/fchollet");
        Account foundAccount = finder.find("fchollet").get(0);

        assertEquals(trueAccount, foundAccount);
    }

}