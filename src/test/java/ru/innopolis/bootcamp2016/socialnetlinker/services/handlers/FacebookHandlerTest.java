package ru.innopolis.bootcamp2016.socialnetlinker.services.handlers;

import org.junit.Test;
import ru.innopolis.bootcamp2016.socialnetlinker.models.Account;
import ru.innopolis.bootcamp2016.socialnetlinker.models.FieldType;
import ru.innopolis.bootcamp2016.socialnetlinker.services.finders.FacebookFinder;
import ru.innopolis.bootcamp2016.socialnetlinker.services.providers.FacebookProvider;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by lemhell on 22.07.16.
 */
public class FacebookHandlerTest {

    @Test
    public void find() throws Exception {
        List<Account> accountList = new FacebookFinder().find("lemhell");

        Map<FieldType, Object> map = new FacebookProvider().provide(accountList.get(0).getLink());
        System.out.println(map.keySet());
    }

}