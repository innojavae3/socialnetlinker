package ru.innopolis.bootcamp2016.socialnetlinker.services.providers;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.innopolis.bootcamp2016.socialnetlinker.models.FieldType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LinkedInProviderTest {
    private LinkedInProvider sut;

    @Before
    public void before(){
        sut = new LinkedInProvider();
    }

    @Test
    public void smokeTest(){
        String link = "олег-кулаев-18272964";

        Map<FieldType, Object> expectedMap = new HashMap<FieldType,Object>();
        expectedMap.put(FieldType.FIRST_NAME,"Олег");
        expectedMap.put(FieldType.SECOND_NAME,"Кулаев");

        ArrayList<String> skills = new ArrayList<String>();
        String[] skillsArray = {"C#","ASP.NET MVC","TypeScript","JavaScript","HDFS",
                "Memcached","MSMQ","Cassandra","HTML","CSS","Unit Testing","WPF",
                "jQuery","KnockoutJS","TeamCity","Mercurial","Microsoft SQL Server",};
        for (String skill : skillsArray){
            skills.add(skill);
        }
        expectedMap.put(FieldType.LI_SKILLS, skills);
        Map<FieldType, Object> actualMap =  sut.provide(link);

        Assert.assertEquals(expectedMap.get(FieldType.FIRST_NAME), actualMap.get(FieldType.FIRST_NAME));
        Assert.assertEquals(expectedMap.get(FieldType.SECOND_NAME), actualMap.get(FieldType.SECOND_NAME));
        Assert.assertEquals(expectedMap.get(FieldType.LI_SKILLS), actualMap.get(FieldType.LI_SKILLS));
    }
}