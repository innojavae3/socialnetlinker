package ru.innopolis.bootcamp2016.socialnetlinker.services.providers;

import org.junit.Test;
import ru.innopolis.bootcamp2016.socialnetlinker.models.FieldType;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class KaggleProviderTest {

    @Test
    // Упадет, если у этого пользователя поменяются данные или произойдет IOException
    public void provideTest() {
        KaggleProvider provider = new KaggleProvider();

        String link = "https://www.kaggle.com/fchollet";
        Map<FieldType, Object> actualUserData = provider.provide(link);
        Map<FieldType, Object> expectedUserData = new HashMap<FieldType, Object>();
        expectedUserData.put(FieldType.KGL_TIER, "master");
        expectedUserData.put(FieldType.KGL_RANK_CUR, 253);
        expectedUserData.put(FieldType.KGL_RANK_MAX, 17);
        expectedUserData.put(FieldType.KGL_RANK_OUT_OF, 46861);

        assertNotNull(actualUserData);
        assertEquals(expectedUserData, actualUserData);
    }

    @Test
    // Упадет, если сменится расположение json'а с данными на странице или его структура
    public void getPageAndUserDataTest() throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        KaggleProvider provider = new KaggleProvider();
        String link = "https://www.kaggle.com/fchollet";

        Method getPage = provider.getClass().getDeclaredMethod("getPage", String.class);
        getPage.setAccessible(true);
        Object page = getPage.invoke(provider, link);

        Method getUserData = provider.getClass().getDeclaredMethod("getUserData", String.class);
        getUserData.setAccessible(true);
        Object userData = getUserData.invoke(provider, (String) page);

        assertNotNull(userData);
    }
}