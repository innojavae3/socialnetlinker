<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SocialNetLinker</title>

    <!-- Bootstrap core CSS -->
    <link href="resources/bootstrap/css/bootstrap.css" rel="stylesheet">

    <link href="resources/css/app.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <h1>SocialNetLinker</h1>

    <div class="">
        <div class="">

            <div class="row">
                <div class="col-sm-offset-3 col-sm-3">
                    <c:if test="${not empty gitHubAcc.PIC_GITHUB}">
                        <img class="img-rounded avatar" src="${gitHubAcc.PIC_GITHUB}">
                    </c:if>
                </div>
                <div class="col-sm-3">
                    <h1><a href="https://github.com/${param.gitHub}">${param.gitHub}</a></h1>
                    <h1>${gitHubAcc.FIRST_NAME} ${gitHubAcc.SECOND_NAME}</h1>
                </div>

            </div>
            <c:choose>
                <c:when test="${not empty accsFields}">

                    <h1 class="text-success col-sm-offset-3">Success!!</h1>
                    <h2 class="col-sm-offset-3">Your result:</h2>

                    <c:forEach items="${accsFields}" var="acc">
                        <c:set var="rank_of" value=""/>
                        <c:set var="img" value=""/>
                        <c:forEach items="${acc.value}" var="entry">
                            <c:if test="${entry.key eq 'KGL_RANK_OUT_OF'}">
                                <c:set var="rank_of" value="${entry.value}"/>
                            </c:if>
                            <c:if test="${entry.key eq 'PIC_FACEBOOK' or entry.key eq 'PIC_LINKEDIN'}">
                                <c:set var="img" value="${entry.value}"/>
                            </c:if>
                        </c:forEach>

                        <div class="col-sm-offset-3 well row">
                            <div class="col-sm-offset-3">
                                <a href="${acc.key.link}">${acc.key.link}</a>
                            </div>
                            <div class="col-sm-3">
                                <c:if test="${not empty img}">
                                    <img class="img-rounded avatar" src="${img}">
                                </c:if>
                            </div>
                            <div class="col-sm-7">
                                <table class="table ">
                                    <c:forEach items="${acc.value}" var="entry">
                                        <%--<c:choose>--%>
                                        <%--<c:when test="${entry.key eq 'KGL_RANK_OUT_OF'}">--%>
                                        <%--<c:set var="rank_of" value="${entry.value}"/>--%>
                                        <%--</c:when>--%>
                                        <c:if test="${not (entry.key eq 'KGL_RANK_OUT_OF' or entry.key eq 'PIC_FACEBOOK' or entry.key eq 'PIC_LINKEDIN')}">
                                            <tr>
                                                <td class="field-name text-capitalize">${fn:toLowerCase(fn:replace(entry.key, "_", " "))}</td>
                                                <td>
                                                        ${entry.value}
                                                    <c:if test="${entry.key eq 'KGL_RANK_CUR' or entry.key eq 'KGL_RANK_MAX'}">
                                                        / ${rank_of}
                                                    </c:if>
                                                </td>
                                            </tr>
                                        </c:if>
                                        <%--</c:choose>--%>

                                    </c:forEach>


                                </table>
                            </div>
                        </div>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <div class="col-sm-offset-3">

                        <h1>Sorry, there is no result for the person: ${param.gitHub}</h1>
                    </div>
                </c:otherwise>
            </c:choose>


        </div> <!-- /container -->
    </div>
</div>
</body>
</html>