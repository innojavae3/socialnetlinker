<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SocialNetLinker</title>

        <!-- Bootstrap core CSS -->
        <link href="resources/bootstrap/css/bootstrap.css" rel="stylesheet">

        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
        <link href="resources/css/app.css" rel="stylesheet">

        <%--<script src="resources/bootstrap/js/bootstrap.js"></script>--%>
        <script src="resources/jquery-3.1.0.js" type="text/javascript"></script>
        <script>
            function find() {
                $.ajax({
                    url: "/account/1",
                    success: function(data) {
                        console.log(data);
                        if (data.found) {
                            $('<a/>', {
                                href: data.link,
                                text: data.link
                            }).appendTo('#acc');
                            getFields();
                        } else {
                            setTimeout(find(), 500);
                        }
                    }
                })/*.done(function(data) {
                    console.log(data);
                });*/

            }

            function getFields() {
                $.ajax({
                    url: "/task/1",
                    success: function(data) {
                        console.log(data);
                        for(var field in data) {
                            var row = $('<tr/>');
                            $('<td/>', {
                                text: field.replace("_", " ").toLowerCase()
                            }).appendTo(row);
                            $('<td/>', {
                                text: data[field]
                            }).appendTo(row);
                            row.appendTo('#result');
                        }
                    }
                })
            }
        </script>
    </head>

    <body onload="find()">

        <div class="container">
            <h1>SocialNetLinker</h1>

            <div class="row">
                <div class="col-sm-offset-3 col-sm-9">

                    <%--<h1 class="text-success">Success!!</h1>--%>
                    <h2>Your result:</h2>

                    <div class="well">
                        <div id="acc"></div>
                        <%--<c:if test="${account.found}">
                            <a href="${account.link}">${account.link}</a>
                        </c:if>--%>
                        <div class="col-sm-offset-5 col-sm-1">
                            <i class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>
                        </div>
                        <table id="result" class="table">
                            <%--<c:forEach items="${fields}" var="entry">
                                <tr>
                                    <td class="field-name text-capitalize">${fn:toLowerCase(fn:replace(entry.key, "_", " "))}</td>
                                    <td>${entry.value}</td>
                                </tr>
                            </c:forEach>--%>
                        </table>
                    </div>

                </div> <!-- /container -->
            </div>
        </div>
    </body>
</html>