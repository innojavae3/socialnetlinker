<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SocialNetLinker</title>

        <!-- Bootstrap core CSS -->
        <link href="resources/bootstrap/css/bootstrap.css" rel="stylesheet">

        <link href="resources/css/app.css" rel="stylesheet">
    </head>

    <body>

        <div class="container">
            <h1>SocialNetLinker</h1>
            <div class="row">

                <form class="col-sm-offset-3 col-sm-6 form-query" action="/result" method="post">
                    <h2 class="form-query-heading">Please enter GitHub account or/and email address</h2>
                    <label for="inputGitHub" class="sr-only">GitHub account</label>
                    <input name="gitHub" type="text" id="inputGitHub" class="form-control" placeholder="GitHub account" required autofocus>
                    <label for="inputEmail" class="sr-only">Email address</label>
                    <input name="email" type="text" id="inputEmail" class="form-control" placeholder="Email address">

                    <button class="btn btn-lg btn-primary btn-block" type="submit">Find the person</button>
                </form>
            </div>

        </div> <!-- /container -->


        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>
