package ru.innopolis.bootcamp2016.socialnetlinker.util;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * PropertiesLoader class is used for project loading properties
 * (usually, from app.properties file).
 */
public class PropertiesLoader {

    @Resource(name = "app.properties")
    private static Properties props = new Properties();

    /**
     * This method returns instance of properties file from resources.
     * @param filename Name of the .properties file
     * @return Properties object, containing key-value property pairs.
     */
    public static Properties getProperties(String filename) {

        try {
            InputStream stream = new FileInputStream("src/main/resources/" + filename);
            props.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return props;
    }

}
