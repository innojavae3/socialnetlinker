package ru.innopolis.bootcamp2016.socialnetlinker.services.finders;

import ru.innopolis.bootcamp2016.socialnetlinker.models.Account;
import yandex.Parser;
import yandex.Result;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * AbstractYandexerFinder is an abstract class, used as a base for
 * searchers through Yandex search system.
 */

public abstract class AbstractYandexerFinder
        implements Finder {

    Parser yandexer;
    String siteParameter;

    /**
     * This method finds a list of accounts, using input data.
     * The search os executed using Yandex search system.
     * @param data Input data to search for.
     * @return List<Account> list of found Accounts.
     */
    public List<Account> find(String data) {
        try {
            yandexer.query(siteParameter + " " + data);
            List<Result> results = yandexer.getResults();
            if (results.size() > 0) {
                return results.stream()
                        .map(r -> new Account(true, r.getUrl()))
                        .collect(Collectors.toCollection(LinkedList::new));
            } else {
                return Arrays.asList(new Account(false, "No results found"));
            }
        } catch (Exception e) {
            return Arrays.asList(new Account(false, e.getMessage()));
        }
    }

}
