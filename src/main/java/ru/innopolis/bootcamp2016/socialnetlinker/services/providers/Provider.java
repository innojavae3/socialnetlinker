package ru.innopolis.bootcamp2016.socialnetlinker.services.providers;

import ru.innopolis.bootcamp2016.socialnetlinker.models.FieldType;

import java.util.Map;

/**
 * Abstract class Provider is used as a base for Providers, which
 * extract the user information from provided links to web pages
 * of users.
 */

public interface Provider {

    /**
     * This method is used to provide user information from
     * provided link to the user's web page.
     * @param link
     * @return
     */
    Map<FieldType, Object> provide(String link);

}
