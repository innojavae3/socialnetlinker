package ru.innopolis.bootcamp2016.socialnetlinker.services.providers;

import ru.innopolis.bootcamp2016.socialnetlinker.models.FieldType;

import java.util.HashMap;
import java.util.Map;

/**
 * This Provider implementation used as a stub in
 * testing purposes.
 */
public class FacebookProviderStub implements Provider {

    public Map<FieldType, Object> provide(String link) {
        HashMap<FieldType, Object> map = new HashMap<>();
        map.put(FieldType.FIRST_NAME, "Mark");
        map.put(FieldType.SECOND_NAME, "Zuckerberg");
        return map;
    }
}
