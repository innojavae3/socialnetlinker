package ru.innopolis.bootcamp2016.socialnetlinker.services.finders;


import ru.innopolis.bootcamp2016.socialnetlinker.models.Account;

import java.util.Arrays;
import java.util.List;

/**
 * This Finder implementation used as a stub in
 * testing purposes.
 */
public class FacebookFinderStub implements Finder {

    /**
     * This method is used to provide synthetic results.
     * @param data Input data to search for.
     * @return List<Account> of found Accounts.
     */
    public List<Account> find(String data) {
        return Arrays.asList(new Account(true, "https://www.facebook.com/zuck"));
    }
}
