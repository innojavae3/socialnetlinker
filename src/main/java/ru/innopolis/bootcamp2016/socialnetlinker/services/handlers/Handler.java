package ru.innopolis.bootcamp2016.socialnetlinker.services.handlers;

import ru.innopolis.bootcamp2016.socialnetlinker.models.Account;
import ru.innopolis.bootcamp2016.socialnetlinker.models.FieldType;
import ru.innopolis.bootcamp2016.socialnetlinker.services.finders.Finder;
import ru.innopolis.bootcamp2016.socialnetlinker.services.providers.Provider;

import java.util.List;
import java.util.Map;

/**
 * Abstract class Handler is used as a base for Handlers, which wrap the
 * functionality of Finders and Providers to search for users and get their
 * personal information like names.
 */

public abstract class Handler
        implements Finder, Provider {

    protected Finder finder;
    protected Provider provider;

    /**
     * This method is used to get links to accounts, found by Finder using
     * the input.
     * @param data Data, with which to search for accounts
     * @return List<Account> - list of found Accounts
     */
    @Override
    public List<Account> find(String data) {
        return finder.find(data);
    }

    /**
     * This method is used to get information about found people,
     * using their Accounts with the Provider, which will try to get the
     * information and return it.
     * @param link Link to the account to get info from.
     * @return Map<FieldType, Object> with information, found with link.
     */
    @Override
    public Map<FieldType, Object> provide(String link) {
        return provider.provide(link);
    }

}
