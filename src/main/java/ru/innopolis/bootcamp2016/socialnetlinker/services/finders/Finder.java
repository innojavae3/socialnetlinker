package ru.innopolis.bootcamp2016.socialnetlinker.services.finders;

import ru.innopolis.bootcamp2016.socialnetlinker.models.Account;

import java.util.List;

/**
 * Abstract class Finder is used as a base for Finders, which
 * search for links to web pages of users, using provided data.
 */

public interface Finder {

    /**
     * This method is used to find all links to accounts,
     * mathching the input data.
     * @param data Input data to search for.
     * @return List<Account> links to accounts, wrapper in Accounts.
     */
    List<Account> find(String data);
}
