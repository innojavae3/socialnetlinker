package ru.innopolis.bootcamp2016.socialnetlinker.services.handlers;

import ru.innopolis.bootcamp2016.socialnetlinker.services.finders.LinkedInFinder;
import ru.innopolis.bootcamp2016.socialnetlinker.services.providers.LinkedInProvider;

/**
 * Created by Admin on 22.07.2016.
 */
public class LinkedInHandler extends Handler {
    public LinkedInHandler() {
        super.finder = new LinkedInFinder();
        super.provider = new LinkedInProvider();
    }
}
