package ru.innopolis.bootcamp2016.socialnetlinker.services.providers;

import ru.innopolis.bootcamp2016.socialnetlinker.models.FieldType;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.*;

/**
 *  Mechanism, used to provide Kaggle account information,
 *  using the link to the web page of the user on Kaggle.
 *
 */
public class KaggleProvider implements Provider {

    /**
     * Finds a user by his link on Kaggle, then parses available
     * information about him.
     *
     * @param link Kaggle UserID
     * @return Map with <FieldType, Object> pairs (Objects usually are strings)
     */
    public Map<FieldType, Object> provide(String link) {
        String page;
        try {
            page = getPage(link);
        }
        catch (IOException e){
            return null;
        }
        JSONObject userData = getUserData(page);

        HashMap<FieldType,Object> selection = new HashMap<FieldType,Object>();

        if (userData != null) {
            selection.put(FieldType.KGL_TIER, userData.get("tier"));
            selection.put(FieldType.KGL_RANK_CUR, userData.get("rankCurrent"));
            selection.put(FieldType.KGL_RANK_MAX, userData.get("rankHighest"));
            selection.put(FieldType.KGL_RANK_OUT_OF, userData.get("rankOutOf"));
        }

        return selection;
    }

    private JSONObject getUserData(String page){
        Pattern userDataJsonPattern =
                Pattern.compile("ReactDOM.render\\(React.createElement\\(KaggleReactComponents\\.ProfileContainerReact, (\\{.*\\})\\),");
        Matcher userDataJsonMatcher = userDataJsonPattern.matcher(page);

        if (userDataJsonMatcher.find()) {
            JSONObject wholeData = new JSONObject(userDataJsonMatcher.group(1));
            return wholeData.getJSONObject("competitionsSummary");
        }

        return null;
    }

    private String getPage(String link) throws IOException {
        String charset = StandardCharsets.UTF_8.name();

        URLConnection connection = new URL(link).openConnection();
        connection.setRequestProperty("Accept-Charset", charset);
        InputStream response = connection.getInputStream();

        Scanner scanner = new Scanner(response);

        return scanner.useDelimiter("\\A").next();
    }
}
