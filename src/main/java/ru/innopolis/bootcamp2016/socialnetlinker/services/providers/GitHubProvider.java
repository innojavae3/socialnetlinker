package ru.innopolis.bootcamp2016.socialnetlinker.services.providers;

import org.kohsuke.github.GHUser;
import org.kohsuke.github.GitHub;
import org.kohsuke.github.GitUser;
import ru.innopolis.bootcamp2016.socialnetlinker.models.FieldType;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static ru.innopolis.bootcamp2016.socialnetlinker.util.PropertiesLoader.getProperties;

/**
 * GitHubProvider is being used for searching user account on github,
 * using provided username, and then extracting the information about
 * the user.
 */
public class GitHubProvider implements Provider {

    private static final Properties props = getProperties("app.properties");
    private static final String GITHUB_TOKEN = props.getProperty("githubToken");

    private static Map<FieldType, Object> githubInfo;

    /**
     * Finds a user by his username on GitHub, then gets his name and surname,
     * and tries to get profile pic, if present and available.
     *
     * @param username Username to search for.
     * @return Map<FieldType, Object> pairs with account info.
     */
    @Override
    public Map<FieldType, Object> provide(String username) {
        Map<FieldType, Object> map = new HashMap<>();
        try {
            GitHub github = GitHub.connect("VagonTramvaev", GITHUB_TOKEN);
            GHUser user = github.getUser(username);
            map = putInfoFromUser(user);
        } catch (IOException e) {
            System.out.println("Error with github authorization: " + e.toString());
            System.out.println("Going to use username: " + username + " for search.");
            map.put(FieldType.FIRST_NAME, username);
        }
        githubInfo = map;
        return map;
    }

    private Map<FieldType, Object> putInfoFromUser(GHUser user) {
        Map<FieldType, Object> map = new HashMap<>();
        try {
            String fullname = user.getName();
            String[] splitName = fullname.split(" ");
            if (splitName.length == 2) {
                map.put(FieldType.FIRST_NAME, splitName[0]);
                map.put(FieldType.SECOND_NAME, splitName[1]);
            } else {
                map.put(FieldType.FIRST_NAME, fullname);
            }
        } catch (Exception ignored) {}

        try {
            String avatarPic = user.getAvatarUrl();
            map.put(FieldType.PIC_GITHUB, avatarPic);
        } catch (Exception ignored) {}

        try {
            String email = user.getEmail();
            map.put(FieldType.EMAIL, email);
        } catch (Exception ignored) {}

        try {
            String company = user.getCompany();
            map.put(FieldType.COMPANY, company);
        } catch (Exception ignored) {}

        try {
            String location = user.getLocation();
            map.put(FieldType.LOCATION, location);
        } catch (Exception ignored) {}
        return map;
    }

}
