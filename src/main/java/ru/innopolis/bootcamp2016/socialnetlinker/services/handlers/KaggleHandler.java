package ru.innopolis.bootcamp2016.socialnetlinker.services.handlers;

import ru.innopolis.bootcamp2016.socialnetlinker.services.finders.KaggleFinder;
import ru.innopolis.bootcamp2016.socialnetlinker.services.providers.KaggleProvider;

/**
 * KaggleHandler is used for searching for people on Kaggle, using
 * username from GitHub.
 *
 * Finds list of links of users by name, then extracts the information
 * from links.
 */

public class KaggleHandler extends Handler {

    public KaggleHandler() {
        super.finder = new KaggleFinder();
        super.provider = new KaggleProvider();
    }

}
