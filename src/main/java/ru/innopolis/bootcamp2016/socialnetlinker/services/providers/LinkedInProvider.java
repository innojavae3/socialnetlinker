package ru.innopolis.bootcamp2016.socialnetlinker.services.providers;

import com.machinepublishers.jbrowserdriver.JBrowserDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ru.innopolis.bootcamp2016.socialnetlinker.models.FieldType;
import ru.innopolis.bootcamp2016.socialnetlinker.models.LinkedInJobData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Владелец on 22.07.16.
 */
public class LinkedInProvider implements Provider {

    private static WebDriver webDriver = new JBrowserDriver();

    static {
        webDriver.get("http://www.linkedin.com");
        webDriver.findElement(By.id("login-email"))
                .sendKeys("vagontramvaev@gmail.com");
        webDriver.findElement(By.id("login-password"))
                .sendKeys("vagontramvaev");
        webDriver.findElement(By.name("submit"))
                .click();
    }

    public LinkedInProvider() {
    }

    /**
     * Finds a user by his UserID on LinkedIn, then gets almost all his data.
     *
     * @param userId LinkedIn UserID
     * @return Map with <FieldType, Object> pairs (Objects are strings, except: LI_SKILLS, LI_CUR_POSITION, LI_PAST_POSITIONS)
     */
    public Map<FieldType, Object> provide(String userId) {
        String linkedIn = "https://www.linkedin.com/in/";
        if (userId.contains(linkedIn)) {
            userId = userId.substring(linkedIn.length());
        }

        Map<FieldType, Object> result = new HashMap<>();

        webDriver.get("https://www.linkedin.com/in/" + userId);

        try {
            String[] firstAndLastName = webDriver.findElement(By.id("name"))
                    .getText()
                    .split("\\s");
            result.put(FieldType.FIRST_NAME, firstAndLastName[0]);
            result.put(FieldType.SECOND_NAME, firstAndLastName[1]);
        } catch (Exception ignored) {}

        try {
            String picLinkedInLink = webDriver.findElement(By.id("control_gen_3"))
                    .findElement(By.tagName("img")).getAttribute("src");
            result.put(FieldType.PIC_LINKEDIN, picLinkedInLink);
        } catch (Exception ignored) {}

        try {
            String twitter = webDriver.findElement(By.id("twitter-view")).findElement(By.tagName("a"))
                    .getAttribute("text");
            result.put(FieldType.TWITTER, twitter);
        } catch (Exception ignored) {}

        try {
            String website = webDriver.findElement(By.id("website-view")).findElement(By.tagName("a"))
                    .getAttribute("href");
            result.put(FieldType.WEBSITE, website);
        } catch (Exception ignored) {}

        try {
            String summary = webDriver.findElement(By.id("summary-item-view")).getText();
            result.put(FieldType.LI_SUMMARY, summary);
        } catch (Exception ignored) {}

        try {
            WebElement cur_position = webDriver.findElement(By.className("current-position"));
            String position = "";
            String company = "";
            String period = "";
            String description = "";

            try {
                position = cur_position.findElement(By.tagName("h4")).findElement(By.tagName("a")).getText();
            } catch (Exception ignored) {}
            try {
                company = cur_position.findElement(By.cssSelector("h5:nth-of-type(2)")).findElement(By.tagName("a")).getText();
            } catch (Exception ignored) {}
            try {
                period = cur_position.findElement(By.className("experience-date-locale")).getText();
            } catch (Exception ignored) {}
            try {
                description = cur_position.findElement(By.className("description")).getText();
            } catch (Exception ignored) {}

            LinkedInJobData cur_pos = new LinkedInJobData(position, company, period, description);

            result.put(FieldType.LI_CUR_POSITION, cur_pos);
        } catch (Exception ignored) {}

        try {
            List<WebElement> past_positions = webDriver.findElements(By.className("past-position"));
            List<LinkedInJobData> past = new ArrayList<LinkedInJobData>();
            for (WebElement pos : past_positions){
                String position = "";
                String company = "";
                String period = "";
                String description = "";

                try {
                    position = pos.findElement(By.tagName("h4")).findElement(By.tagName("a")).getText();
                } catch (Exception ignored) {}
                try {
                    company = pos.findElement(By.cssSelector("h5:nth-of-type(2)")).findElement(By.tagName("a")).getText();
                } catch (Exception ignored) {}
                try {
                    period = pos.findElement(By.className("experience-date-locale")).getText();
                } catch (Exception ignored) {}
                try {
                    description = pos.findElement(By.className("description")).getText();
                } catch (Exception ignored) {}

                past.add(new LinkedInJobData(position, company, period, description));
            }

            result.put(FieldType.LI_PAST_POSITIONS, past);
        } catch (Exception ignored) {}

        try{
            List<WebElement> skills = webDriver.findElements(By.className("endorse-item-name-text"));
            List<String> skillsList = new ArrayList<String>();
            for (WebElement skill : skills) {
                skillsList.add(skill.getText());
            }
            result.put(FieldType.LI_SKILLS, skillsList);
        } catch (Exception ignored) {}

        return result;
    }
}
