package ru.innopolis.bootcamp2016.socialnetlinker.services.finders;

import yandex.Parser;
import java.util.Properties;
import static ru.innopolis.bootcamp2016.socialnetlinker.util.PropertiesLoader.getProperties;

/**
 * LinkedInFinder is being used to find LinkedIn account, using
 * possible account name. The search is executed using implementation
 * of AbstractYandexerFinder.
 */

public class LinkedInFinder
        extends AbstractYandexerFinder
        implements Finder {

    private static Properties props = getProperties("app.properties");

    public LinkedInFinder() {
        super.yandexer = new Parser(
                props.getProperty("yandexUsername"),
                props.getProperty("yandexPassword"));

        super.siteParameter = "site:linkedin.com/in";
    }

}
