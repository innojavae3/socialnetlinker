package ru.innopolis.bootcamp2016.socialnetlinker.services.finders;

import com.restfb.*;
import com.restfb.types.User;
import ru.innopolis.bootcamp2016.socialnetlinker.models.Account;
import ru.innopolis.bootcamp2016.socialnetlinker.models.FieldType;
import ru.innopolis.bootcamp2016.socialnetlinker.services.providers.GitHubProvider;
import java.util.*;
import java.util.stream.Collectors;

import static ru.innopolis.bootcamp2016.socialnetlinker.util.PropertiesLoader.getProperties;

/**
 *  Mechanism, used to provide Facebook account links via the Facebook search,
 *  using the username, which can be nickname or the First Name + Second Name combination.
 *
 */
public class FacebookFinder implements Finder {

    private static final Properties props = getProperties("app.properties");
    private static final String FACEBOOK_TOKEN = props.getProperty("facebookUserAccessToken");

    private static List<String> search(String username) {
        Map<FieldType, Object> githubInfo = new GitHubProvider().provide(username);
        String fullname;
        if (githubInfo.containsKey(FieldType.SECOND_NAME)) {
            fullname = githubInfo.get(FieldType.FIRST_NAME) + " " + githubInfo.get(FieldType.SECOND_NAME);
        } else {
            fullname = (String)githubInfo.get(FieldType.FIRST_NAME);
        }
        FacebookClient facebookClient = new DefaultFacebookClient(FACEBOOK_TOKEN, Version.VERSION_2_6);

        Connection<User> targetedSearch = facebookClient.fetchConnection("search", User.class,
                Parameter.with("q", fullname), Parameter.with("type", "user"));
        List<User> data = targetedSearch.getData();
        List<String> links = new ArrayList<>();
        try {
            links.addAll(data.stream().map(u -> "https://www.facebook.com/" + u.getId()).collect(Collectors.toList()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return links;
    }

    /**
     * Finds all links to the Facebook users with matching usernames, using
     * Facebook search. Wraps them in instances of Account.
     *
     * @param username Facebook UserID
     * @return List<Account> list of user accounts with matching usernames
     */

    public List<Account> find(String username) {
        List<String> links = search(username);
        return links.stream().map(Account::new).collect(Collectors.toList());
    }

}
