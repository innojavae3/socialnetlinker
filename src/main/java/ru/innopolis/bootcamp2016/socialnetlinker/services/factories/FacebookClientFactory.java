package ru.innopolis.bootcamp2016.socialnetlinker.services.factories;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Version;

import java.util.Properties;

import static ru.innopolis.bootcamp2016.socialnetlinker.util.PropertiesLoader.getProperties;

public class FacebookClientFactory {

    private static final Properties props = getProperties("app.properties");
    private static final String APP_ID = props.getProperty("facebookAppId");
    private static final String APP_SECRET = props.getProperty("facebookAppSecret");

    public FacebookClient getClient() {
        System.out.println(APP_ID);
        System.out.println(APP_SECRET);
        return new DefaultFacebookClient(getAccessToken(), APP_SECRET, Version.LATEST);
    }

    private String getAccessToken() {
        FacebookClient fbClient = new DefaultFacebookClient(Version.LATEST);
        return fbClient.obtainAppAccessToken(APP_ID, APP_SECRET).getAccessToken();
    }

}
