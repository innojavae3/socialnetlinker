package ru.innopolis.bootcamp2016.socialnetlinker.services.finders;

import yandex.Parser;
import java.util.Properties;
import static ru.innopolis.bootcamp2016.socialnetlinker.util.PropertiesLoader.getProperties;

/**
 * KaggleFinder is being used to find Kaggle account, using
 * possible account name. The search is executed using implementation
 * of AbstractYandexerFinder.
 */

public class KaggleFinder
        extends AbstractYandexerFinder
        implements Finder {

    private Properties props;

    public KaggleFinder() {
        props = getProperties("app.properties");
        super.yandexer = new Parser(
                props.getProperty("yandexUsername"),
                props.getProperty("yandexPassword"));

        super.siteParameter = "site:kaggle.com intext:Active";
    }

}
