package ru.innopolis.bootcamp2016.socialnetlinker.services.handlers;

import ru.innopolis.bootcamp2016.socialnetlinker.services.finders.FacebookFinder;
import ru.innopolis.bootcamp2016.socialnetlinker.services.finders.FacebookFinderStub;
import ru.innopolis.bootcamp2016.socialnetlinker.services.providers.FacebookProvider;
import ru.innopolis.bootcamp2016.socialnetlinker.services.providers.FacebookProviderStub;

/**
 * FacebookHandler is used for searching for people on Facebook, using their
 * names directly or via the GitHubProvider, which searches for names by
 * usernames and then continues the search on Facebook.
 *
 * Finds list of links of users by name, then extracts the information
 * from links.
 */

public class FacebookHandler extends Handler {

    public FacebookHandler() {
        super.finder = new FacebookFinder();
        super.provider = new FacebookProvider();
    }

}
