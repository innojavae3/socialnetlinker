package ru.innopolis.bootcamp2016.socialnetlinker.services.providers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ru.innopolis.bootcamp2016.socialnetlinker.models.FieldType;
import com.machinepublishers.jbrowserdriver.JBrowserDriver;
import java.util.HashMap;
import java.util.Map;

/**
 *  Mechanism, used to provide Facebook account information,
 *  using the id, which points to user's web-page, or the link to
 *  the page itself.
 *
 */
public class FacebookProvider implements Provider {

    private static WebDriver webDriver = new JBrowserDriver();

    static {
        webDriver.get("http://facebook.com");
        webDriver.findElement(By.id("email"))
                .sendKeys("vagontramvaev@gmail.com");
        webDriver.findElement(By.id("pass"))
                .sendKeys("vagontramvaev");
        webDriver.findElement(By.id("loginbutton"))
                .click();
    }

    public FacebookProvider() {
    }

    /**
     * Finds a user by his UserID on Facebook, then gets his name and surname,
     * and tries to get email and profile pic, if present and available.
     *
     * @param userId Facebook UserID
     * @return Map with <FieldType, Object> pairs (Objects usually are strings)
     */
    public Map<FieldType, Object> provide(String userId) {
        String facebook = "https://www.facebook.com/";
        if (userId.contains(facebook)) {
            userId = userId.substring(facebook.length());
        }

        Map<FieldType, Object> result = new HashMap<>();

        webDriver.get("https://www.facebook.com/" + userId);

        try {
            String[] firstAndLastName = webDriver.findElement(By.id("fb-timeline-cover-name"))
                    .getText()
                    .split("\\s");
            result.put(FieldType.FIRST_NAME, firstAndLastName[0]);
            result.put(FieldType.SECOND_NAME, firstAndLastName[1]);
        } catch (Exception ignored) {
            System.out.println("exc name ");
        }
        webDriver.get("https://www.facebook.com/" + userId);

        try {
            String picFacebookLink = webDriver.findElement(By.className("profilePic"))
                    .getAttribute("src");
            result.put(FieldType.PIC_FACEBOOK, picFacebookLink);
        } catch (Exception ignored) {
            System.out.println("exc pic");
        }

        try {
            String emailText = webDriver.findElement(By.xpath("a[href^='mailto:']"))
                    .getText();
            result.put(FieldType.EMAIL, emailText);
        } catch (Exception ignored) {
            System.out.println("exc mail");
        }

        return result;
    }
}
