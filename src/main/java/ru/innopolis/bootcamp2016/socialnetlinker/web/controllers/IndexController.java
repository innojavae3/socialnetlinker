package ru.innopolis.bootcamp2016.socialnetlinker.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.bootcamp2016.socialnetlinker.models.Account;
import ru.innopolis.bootcamp2016.socialnetlinker.models.FieldType;
import ru.innopolis.bootcamp2016.socialnetlinker.services.handlers.FacebookHandler;
import ru.innopolis.bootcamp2016.socialnetlinker.services.handlers.Handler;
import ru.innopolis.bootcamp2016.socialnetlinker.services.handlers.KaggleHandler;
import ru.innopolis.bootcamp2016.socialnetlinker.services.handlers.LinkedInHandler;
import ru.innopolis.bootcamp2016.socialnetlinker.services.providers.GitHubProvider;
import ru.innopolis.bootcamp2016.socialnetlinker.services.providers.Provider;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
public class IndexController {

    private Provider gitHub = new GitHubProvider();
    private Handler[] handlers = new Handler[]{
            new LinkedInHandler(),
            new KaggleHandler(),
            new FacebookHandler()
    };

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String indexGet(Model model) {
        return "index";
    }

    @RequestMapping(value = "/result", method = RequestMethod.POST)
    public String resultPage(Model model, @RequestParam("gitHub") String query, @RequestParam(value = "email", required = false) String email) {
        Map<FieldType, Object> gitFields = gitHub.provide(query);
        Map<String, Object> gitStrings = new LinkedHashMap<>();
        gitFields.forEach((FieldType fieldType, Object object) -> {
            gitStrings.put(fieldType.toString(), object);
        });
        model.addAttribute("gitHubAcc", gitStrings);

        final int resultsCount = 7;
        Map<Account, Map<FieldType, Object>> accsFields = new LinkedHashMap<>();
        for (Handler handler : handlers) {
            try {
                List<Account> accounts = handler.find(query);
                if (email != null && !email.trim().isEmpty()) {
                    email = email.trim();
                    int atIdx = email.indexOf("@");
                    String username = email.substring(0, atIdx > 0 ? atIdx : email.length());
                    List<Account> byEmail = handler.find(username);
                    List<Account> res = new ArrayList<>();
                    accounts.forEach(res::add);
                    byEmail.forEach((e) -> {
                        if (!res.contains(e)) { 
                            res.add(e);
                        }
                    });
                    accounts = res;
                }
                accounts.subList(0, Math.min(accounts.size(), resultsCount)).stream()
                        .filter(Account::isFound)
                        .forEach(acc ->
                                accsFields.put(acc, handler.provide(acc.getLink())));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        model.addAttribute("accsFields", accsFields);
        return "result";
    }

}
