package ru.innopolis.bootcamp2016.socialnetlinker.models;

/**
 * FieldType enumeration is used as a marker of
 * some type of data, returned by the Providers.
 */
public enum FieldType {
    FIRST_NAME,
    SECOND_NAME,
    PHONE_NUMBER,
    GENDER,
    ABOUT,
    RELIGION,
    EMAIL,
    COMPANY,
    LOCATION,
    AGE,
    BIO,
    BIRTHDAY,
    HOMETOWN,

    PIC_GITHUB,
    PIC_FACEBOOK,
    PIC_LINKEDIN,

    TWITTER,
    WEBSITE,

    KGL_TIER,
    KGL_RANK_CUR,
    KGL_RANK_MAX,
    KGL_RANK_OUT_OF,

    LI_SUMMARY,
    LI_PAST_POSITIONS,
    LI_CUR_POSITION,
    LI_JOB_POSITION,
    LI_JOB_COMPANY,
    LI_JOB_PERIOD,
    LI_JOB_DESCRIPTION,
    LI_SKILLS,
}
