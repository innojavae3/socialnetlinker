package ru.innopolis.bootcamp2016.socialnetlinker.models;

import java.util.HashMap;

public class LinkedInJobData {
    private HashMap<FieldType,String> job = new HashMap<FieldType,String>();

    public LinkedInJobData(String position, String company, String period, String description){
        job.put(FieldType.LI_JOB_POSITION, position);
        job.put(FieldType.LI_JOB_COMPANY, company);
        job.put(FieldType.LI_JOB_PERIOD, period);
        job.put(FieldType.LI_JOB_DESCRIPTION, description);
    }

    public HashMap<FieldType,String> getJob() {
        return job;
    }

    public String toString() {
        return  job.get(FieldType.LI_JOB_POSITION) + "\n" +
            job.get(FieldType.LI_JOB_COMPANY) + "\n" +
            job.get(FieldType.LI_JOB_PERIOD) + "\n" +
            job.get(FieldType.LI_JOB_DESCRIPTION);
    }
}
