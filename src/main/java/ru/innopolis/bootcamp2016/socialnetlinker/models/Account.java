package ru.innopolis.bootcamp2016.socialnetlinker.models;

/**
 * Account class is a wrapper for the link to the user account
 * and flag, that the accound was found (or not found, in this case
 * the link will be null.
 */

public class Account {

    private boolean found;
    private String link;

    public Account() {
    }

    public Account(String link) {
        this(true, link);
    }

    public Account(boolean found, String link) {
        this.found = found;
        this.link = link;
    }

    public boolean isFound() {
        return this.found;
    }

    public void setFound(boolean wasFound) {
        this.found = wasFound;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Account account = (Account) o;

        return found == account.found &&
                (link != null ?
                        link.equals(account.link) : account.link == null);

    }

    @Override
    public String toString() {
        return "Account{" +
                "found=" + found +
                ", link='" + link + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        int result = (found ? 1 : 0);
        result = 31 * result + (link != null ? link.hashCode() : 0);
        return result;
    }
}
